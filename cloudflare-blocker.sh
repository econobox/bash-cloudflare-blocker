#!/bin/bash

# Copyright (C) 2019 Robin Wils

# This script is free software: you can redistribute it and/or modify 
# it under the terms of the GNU General Public License as published by 
# the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version. 

# This script is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
# GNU General Public License for more details. 

# You should have received a copy of the GNU General Public License 
# along with this script.  If not, see <https://www.gnu.org/licenses/>.

# CONFIG

# File with the Cloudflare ipv4s
readonly CLOUDFLARE_IPV4S=`cat cloudflare-ipv4s.txt`
# File with the Cloudflare ipv6s
readonly CLOUDFLARE_IPV6S=`cat cloudflare-ipv6s.txt`
# Location of iptables firewall
readonly IPTABLES=/sbin/iptables
# Location of ip6tables firewall
readonly IP6TABLES=/sbin/ip6tables


# SCRIPT

# You have to be root.
if [ "$EUID" -ne 0 ]
then echo "You need to be root to run iptables."
     exit 1
fi

# Block Cloudflare if we don't receive arguments
iptables_argument="-A"

# Check arguments
while getopts ":jl" option;
do
    case $1 in 
        -block)
	          iptables_argument="-A"
            break
	          ;;
        -unblock)
	          iptables_argument="-D"
            break
            ;;
        -help)
            echo "Available arguments: block, unblock, help"
            break
	          ;;
        *)
            echo "No valid argument detected."
            echo "Available arguments: block, unblock, help"
            break
	          ;;
    esac
done

# Set iptables
for ip in $CLOUDFLARE_IPV4S
do
    $IPTABLES $iptables_argument INPUT -s $ip -j DROP
done

# Set ip6tables
for ip in $CLOUDFLARE_IPV6S
do
    $IP6TABLES $iptables_argument INPUT -s $ip -j DROP
done

exit
